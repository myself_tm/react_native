// @flow

// Diagnose item in ChartView

import React from 'react'
import {
  View,
  Text,
  TouchableOpacity
} from 'react-native'
import styles from './Styles/ChartViewDiagnoseStyle'
import ChartViewParagraphs from './ChartViewParagraphs'

const TABS = ['basics', 'diagnosis', 'treatment', 'links']
const BASICS = {
  description: 'Description',
  epidemiology: {
    title: 'Epidemiology',
    subTitles: {
      predominantSex: 'Predominant gender',
      predominantAgeRange: 'Predominant age range',
      prevalence: 'Prevalence'
    }
  },
  etiology: 'Etiology',
  genetics: 'Genetics',
  riskFactor: 'Risk factors',
  generalPrevention: 'General prevention',
  commonlyAssociatedConditions: 'Commonly associated conditions',
  associatedSymptoms: 'Associated symptoms'
}
const DIAGNOSIS = {
  history: 'History',
  physicalExamination: 'Physical examination',
  diagnosis: {
    title: 'Diagnosis',
    subTitles: {
      testBlood: 'Lab tests',
      testImaging: 'Imaging tests',
      testOther: 'Other tests'
    }
  },
  differentialDiagnosis: 'Differential diagnosis'
}
const TREATMENT = {
  followUp: 'Follow up',
  monitoring: 'Monitoring',
  diet: 'Diet',
  prognosis: 'Prognosis'
}
const LINKS = {
  links: 'Links'
}

export default class ChartViewDiagnose extends React.Component {
  constructor () {
    super()

    this.state = {
      activeTab: 0
    }
  }

  handleTabSelect = (index) => {
    this.setState({activeTab: index})
  }

  getTabs = () => {
    const {activeTab} = this.state

    return TABS.map((tab, i) => {
      return (
        <View key={i} style={[styles.tabContainer, i === 3 && {borderRightWidth: 0}]}>
          <TouchableOpacity
            onPress={() => this.handleTabSelect(i)}
            style={[styles.tabTouch, activeTab === i && styles.tabTouchActive]}
          >
            <Text style={[styles.tab, activeTab === i && styles.tabActive]}>
              {tab.toUpperCase()}
            </Text>
          </TouchableOpacity>
        </View>
      )
    })
  }

  getDifferentialDiagnosis = () => {
    return this.props.diseasesReference.map(item => item.name).join(`\n`)
  }

  getTabContent = () => {
    const diagnosis = this.getDifferentialDiagnosis()
    const diagnosisData = {...this.props.data, differentialDiagnosis: diagnosis}

    switch (this.state.activeTab) {
      case 0:
        return <ChartViewParagraphs data={this.props.data} titles={BASICS} />
      case 1:
        return <ChartViewParagraphs data={diagnosisData} titles={DIAGNOSIS} />
      case 2:
        return <ChartViewParagraphs data={this.props.data} titles={TREATMENT} />
      case 3:
        return <ChartViewParagraphs data={this.props.diseasesLinks} titles={LINKS} />
    }
  }

  render () {
    return (
      <View style={styles.container}>
        <View style={styles.tabsContainer}>
          {this.getTabs()}
        </View>
        <View style={styles.contentContainer}>
          {this.getTabContent()}
        </View>
      </View>
    )
  }
}
