// @flow

import React, { Component } from 'react'
import { Scene, Router } from 'react-native-router-flux'
import LoginScreen from '../Containers/LoginScreen'
import TutorialScreen from '../Containers/TutorialScreen'
import ExaminationScreen from '../Containers/ExaminationScreen'
import AddCustomScreen from '../Containers/AddCustomScreen'
import OurStoryScreen from '../Containers/OurStoryScreen'
import SettingsScreen from '../Containers/SettingsScreen'

/* **************************
* Documentation: https://github.com/aksonov/react-native-router-flux
***************************/

class NavigationRouter extends Component {

  render () {
    const {initialScreen} = this.props
    return (
      <Router backAndroidHandler={() => false}>
        <Scene initial={initialScreen === 'login'} key='login' component={LoginScreen} hideNavBar />
        <Scene initial={initialScreen === 'tutorial'} key='tutorial' component={TutorialScreen} hideNavBar />
        <Scene initial={initialScreen === 'examination'} key='examination' component={ExaminationScreen} hideNavBar />
        <Scene key='addCustom' component={AddCustomScreen} hideNavBar />
        <Scene key='ourStory' component={OurStoryScreen} hideNavBar />
        <Scene key='settings' component={SettingsScreen} hideNavBar />
      </Router>
    )
  }
}

export default NavigationRouter
