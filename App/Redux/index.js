// @flow

import { combineReducers } from 'redux'
import configureStore from './CreateStore'
import rootSaga from '../Sagas/'

export default () => {
  /* ------------- Assemble The Reducers ------------- */
  const rootReducer = combineReducers({
    findingOutputs: require('./FindingOutputsRedux').reducer,
    units: require('./UnitsRedux').reducer,
    login: require('./LoginRedux').reducer,
    search: require('./SearchRedux').reducer,
    tutorial: require('./TutorialRedux').reducer,
    findings: require('./FindingsRedux').reducer,
    listItems: require('./ListItemsRedux').reducer,
    icd10: require('./Icd10Redux').reducer,
    disease: require('./DiseaseRedux').reducer,
    planning: require('./PlanningRedux').reducer,
    customPlanning: require('./CustomPlanningRedux').reducer,
    customDrugs: require('./CustomDrugsRedux').reducer,
    findingGroups: require('./FindingGroupsRedux').reducer,
    diseasesReference: require('./DiseasesReferenceToDiseasesRedux').reducer,
    diseasesLinks: require('./DiseasesLinksRedux').reducer,
    drug: require('./DrugRedux').reducer,
    diagnose: require('./DiagnoseRedux').reducer,
    userState: require('./UserStateRedux').reducer
  })

  return configureStore(rootReducer, rootSaga)
}
