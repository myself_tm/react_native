// @flow

// leave off @2x/@3x
const images = {
  // login
  logo: require('../Images/logo.png'),
  emailIcon: require('../Images/email_icon.png'),
  passIcon: require('../Images/pass_icon.png'),
  checkBox: require('../Images/box_icon.png'),
  checkBoxActive: require('../Images/box_checked_icon.png'),

  // introTutorial
  diagnoseBg: require('../Images/diagnose_bg.png'),
  editBg: require('../Images/edit_bg.png'),
  matchingBg: require('../Images/matching_bg.png'),
  tooltipBg: require('../Images/tooltip_bg.png'),

  // examination
  menu: require('../Images/menu.png'),
  menuLogo: require('../Images/menu_logo.png'),
  tree: require('../Images/tree.png'),
  search: require('../Images/search.png'),
  add: require('../Images/add.png'),
  go_btn: require('../Images/go_btn.png'),
  go_btn_input: require('../Images/go_btn_input.png'),
  go_btn_selected: require('../Images/go_btn_selected.png'),
  more: require('../Images/more.png'),
  expand: require('../Images/more_icon.png'),
  diagnose: require('../Images/diag.png'),
  findings: require('../Images/findings_icon.png'),
  planning: require('../Images/planning_icon.png'),
  treatment: require('../Images/treatment_icon.png'),
  addBtn: require('../Images/add_btn_icon.png'),
  radio: require('../Images/select_icon.png'),
  radioActive: require('../Images/select_active_icon.png'),
  back: require('../Images/back.png'),
  arrowRight: require('../Images/arrow.png'),
  trash: require('../Images/del_btn_icon.png')
}

export default images
