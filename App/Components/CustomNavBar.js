// Nav bar in Examination, AddCustom, OurStory, Settings screens

import React from 'react'
import NavBar from 'react-native-navbar'
import NavBarButton from './NavBarButton'
import styles from './Styles/CustomNavBarStyle'

export default class CustomNavBar extends React.Component {
  getLeftButton = () => {
    if (this.props.leftButtonIcon) {
      return (
        <NavBarButton handler={this.props.leftButtonHandler}>
          {this.props.leftButtonIcon}
        </NavBarButton>
      )
    } else return null
  }

  getTitle = () => {
    if (this.props.title) {
      return {
        title: this.props.title.toUpperCase(),
        style: styles.title
      }
    } else return null
  }

  getRightButton = () => {
    if (this.props.rightButtonIcon) {
      return (
        <NavBarButton handler={this.props.rightButtonHandler}>
          {this.props.rightButtonIcon}
        </NavBarButton>
      )
    } else return null
  }

  render () {
    return (
      <NavBar
        leftButton={this.getLeftButton()}
        title={this.getTitle()}
        rightButton={this.getRightButton()}
        style={styles.navBar}
        statusBar={{hidden: true}}
      />
    )
  }
}

CustomNavBar.propTypes = {
  leftButtonIcon: React.PropTypes.object,
  leftButtonHandler: React.PropTypes.func,
  title: React.PropTypes.string,
  rightButtonIcon: React.PropTypes.object,
  rightButtonHandler: React.PropTypes.func
}

CustomNavBar.defaultProps = {
  leftButtonIcon: null,
  leftButtonHandler: () => {},
  title: '',
  rightButtonIcon: null,
  rightButtonHandler: () => {}
}
