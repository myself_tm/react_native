// @flow

// Drug item in ExaminationScreen -> SelectedList

import React from 'react'
import { View, Text, Image, TextInput, TouchableOpacity } from 'react-native'
import Immutable from 'seamless-immutable'
import styles from './Styles/DrugStyle'
import { Colors, Images, Metrics } from '../Themes'

const INPUTS = ['0', '1', '2', '3']

export default class Drug extends React.Component {
  constructor (props) {
    super(props)

    this.id = props.id
    this.planId = props.data.treatmentPlanId
    this.isDrug = !!props.data.treatmentPlanDrugVariation
    this.data = this.isDrug
      ? props.data.treatmentPlanDrugVariation.drugVariation[0] || props.data.treatmentPlanDrugVariation.drugVariation
      : props.data

    this.state = {
      values: this.setStartState('values', props),
      dose: this.setStartState('dose', props),
      isExpanded: false
    }
  }

  componentWillMount () {
    this.handleUserInputs()
  }

  setStartState = (property, props) => {
    const {diagnoseDrugs} = props

    if (diagnoseDrugs[this.planId] && diagnoseDrugs[this.planId][property]) {
      return diagnoseDrugs[this.planId][property]
    } else if (property === 'values') {
      const {morningDose, lunchDose, afternoonDose, eveningDose} = this.props.data.treatmentPlanDrugVariation
      return [morningDose, lunchDose, afternoonDose, eveningDose]
    } else {
      const {treatmentPlanDrugVariationTranslations} = this.props.data.treatmentPlanDrugVariation
      let dosageText = ''
      treatmentPlanDrugVariationTranslations.forEach(translate => {
        if (translate.locale === 'en') {
          dosageText = translate.dosageText
        }
      })
      return dosageText
    }
  }

  handleValue = (number, index) => {
    const _number = number.trim()

    if (isNaN(_number)) { return }

    let _values = Immutable.asMutable(this.state.values)
    _values.splice(index, 1, number)
    this.setState({values: _values})
  }

  handleDose = (text) => {
    this.setState({dose: text})
  }

  handleUserInputs = () => {
    const {diagnoseDrugs} = this.props

    this.props.selectDrug({
      [this.planId]: {
        ...diagnoseDrugs[this.planId],
        values: this.state.values,
        dose: this.state.dose
      }
    })
  }

  getSubTitle = () => {
    if (this.data.unit) {
      return (
        <View style={styles.subTitleContainer}>
          <Text style={styles.subTitle}>
            {this.data.strength}
            {this.data.unit.name}, {this.data.packageSize}{this.data.drugForm.name}
          </Text>
        </View>
      )
    } else {
      return null
    }
  }

  getInputs = () => {
    if (this.isDrug) {
      return INPUTS.map(index => {
        return (
          <TextInput
            key={index}
            ref={(ref) => { this[`input${index}`] = ref }}
            onFocus={() => { Metrics.isIos && this.props.scrollToInput(this[`input${index}`]) }}
            autoCorrect={false}
            style={[styles.input, styles.inputValues]}
            maxLength={2}
            value={this.state.values[index]}
            onChangeText={(text) => this.handleValue(text, index)}
            onBlur={this.handleUserInputs}
            placeholder='0'
            placeholderTextColor={Colors.ghost}
            underlineColorAndroid={Colors.transparent}
            onSubmitEditing={() => {
              Metrics.isIos && setTimeout(() => this.props.scrollRef.scrollToEnd(), 100)
            }}
            returnKeyType='done'
          />
        )
      })
    }
  }

  render () {
    const {isExpanded} = this.state
    const placeholder = this.isDrug ? 'Dose text' : 'Details'

    return (
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          <Text style={styles.title} numberOfLines={1} ellipsizeMode='tail'>
            {this.data.name || this.data.drug.name}
          </Text>
          <TouchableOpacity
            style={styles.expandIconTouch}
            onPress={() => this.setState({isExpanded: !this.state.isExpanded})}
          >
            <Image
              source={Images.expand}
              style={[styles.expandIcon, isExpanded && styles.expandIconActive]}
            />
          </TouchableOpacity>
        </View>
        {this.getSubTitle()}

        {isExpanded &&
          <View style={styles.inputsContainer}>
            <View style={styles.inputsLine}>
              {this.getInputs()}
            </View>
            <TextInput
              ref={ref => { this.doseInput = ref }}
              onFocus={() => { Metrics.isIos && this.props.scrollToInput(this.doseInput) }}
              autoCorrect={false}
              style={[styles.input, styles.inputDose]}
              defaultValue={this.state.dose}
              value={this.state.dose}
              onChangeText={(text) => this.handleDose(text)}
              onBlur={this.handleUserInputs}
              placeholder={placeholder}
              placeholderTextColor={Colors.ghost}
              underlineColorAndroid={Colors.transparent}
              onSubmitEditing={() => { Metrics.isIos && this.props.scrollRef.scrollToEnd() }}
              returnKeyType='done'
            />
          </View>
        }
      </View>
    )
  }
}
