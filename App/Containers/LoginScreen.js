// @flow

// Start screen with login logic

import React from 'react'
import {
  View,
  Text,
  Image,
  Alert,
  Modal,
  Platform,
  Keyboard,
  TextInput,
  ScrollView,
  TouchableOpacity,
  KeyboardAvoidingView
} from 'react-native'
import { connect } from 'react-redux'
import { Images, Colors, Fonts, ApplicationStyles } from '../Themes'
import SplashScreen from 'react-native-splash-screen'
import StatusBarIos from '../Components/StatuBarIos'
import Options from '../Components/Options'
import LoginActions from '../Redux/LoginRedux'
import FindingsActions from '../Redux/FindingsRedux'
import ListItemsActions from '../Redux/ListItemsRedux'
import DiseaseActions from '../Redux/DiseaseRedux'
import ForgotPasswordModal from '../Components/ForgotPasswordModal'
import UnitsActions from '../Redux/UnitsRedux'
import FindingOutputsActions from '../Redux/FindingOutputsRedux'

// Styles
import styles from './Styles/LoginScreenStyle'

const INPUT_HEIGHT = 70
const EMAIL_REGEX = /.+@.+[.].+/

class LoginScreen extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      emailValue: props.email || '',
      isEmail: !!props.email,
      emailError: false,
      passwordValue: '',
      isPasswordLength: false,
      passwordError: false,
      isRemember: false,
      keyboardOffset: 0,
      isForgotModalVisible: false
    }
  }

  componentDidMount () {
    SplashScreen.hide()
  }

  handleEmail = (text) => {
    const isEmail = EMAIL_REGEX.test(text)

    this.setState({
      emailValue: text,
      emailError: false
    })
    if (isEmail) {
      this.setState({isEmail: true})
    } else {
      this.setState({isEmail: false})
    }
  }

  handlePassword = (text) => {
    const isPasswordLength = text.length > 0

    this.setState({
      passwordValue: text,
      passwordError: false
    })
    if (isPasswordLength) {
      this.setState({isPasswordLength})
    } else {
      this.setState({isPasswordLength})
    }
  }

  handleRememberPress = () => {
    this.setState({isRemember: !this.state.isRemember})
  }

  showForgotModal = () => {
    this.setState({isForgotModalVisible: true})
  }

  hideForgotModal = () => {
    this.setState({isForgotModalVisible: false})
  }

  setKeyboardOffset = (value) => {
    if (Platform.OS !== 'ios') {
      this.setState({keyboardOffset: value})
    }
  }

  handleLogin = () => {
    const {isEmail, isPasswordLength} = this.state
    const nextPage = this.props.showTutorial
      ? 'tutorial'
      : 'examination'

    if (!isEmail) {
      this.setState({emailError: true})
      return
    } else if (!isPasswordLength) {
      this.setState({passwordError: true})
      return
    } else {
      this.props.getUnits()
      this.props.getListItems()
      this.props.getFindings()
      this.props.getFindingOutputs()
      this.props.attemptLogin(
        this.state.emailValue.toLowerCase().trim(),
        this.state.passwordValue.trim(),
        this.state.isRemember,
        () => Alert.alert(
            '',
            'Credentials are incorrect',
            [{text: 'OK', onPress: () => {}}]
        ),
        nextPage
      )
    }

    Keyboard.dismiss()
  }

  render () {
    const {emailValue, isEmail, emailError, passwordValue, isPasswordLength, passwordError} = this.state
    const emailIconStyles = [
      styles.emailIcon,
      isEmail && !emailError && {tintColor: Colors.danube},
      emailError && {tintColor: Colors.roman}
    ]
    const passIconStyles = [
      styles.passIcon,
      isPasswordLength && !passwordError && {tintColor: Colors.danube},
      passwordError && {tintColor: Colors.roman}
    ]
    const isCredentials = isEmail && isPasswordLength

    return (
      <View style={styles.scrollContainer}>
        <StatusBarIos light />
        <ScrollView
          contentContainerStyle={styles.scrollContainer}
          keyboardShouldPersistTaps='always'
        >
          <KeyboardAvoidingView
            contentContainerStyle={styles.scrollContainer}
            style={styles.scrollContainer}
            behavior='position'
            keyboardVerticalOffset={this.state.keyboardOffset}
          >
            <View style={styles.container}>
              <View style={styles.logoContainer}>
                <Image
                  source={Images.logo}
                  {...ApplicationStyles.screenWidthLogo} />
              </View>
              <View style={styles.loginContainer}>
                <View style={[styles.inputContainer, styles.emailInputContainer]}>
                  <Image
                    source={Images.emailIcon}
                    style={emailIconStyles}
                  />
                  <TextInput
                    autoCorrect={false}
                    onChangeText={text => this.handleEmail(text)}
                    placeholder={'user email'.toUpperCase()}
                    placeholderTextColor={Colors.ghost}
                    value={this.state.emailValue}
                    defaultValue={this.props.email}
                    underlineColorAndroid={Colors.transparent}
                    style={[styles.input, styles.emailInput, emailValue && {fontFamily: Fonts.type.bold}]}
                  />
                </View>
                <View style={[styles.inputContainer, styles.passInputContainer]}>
                  <View style={styles.passIconContainer}>
                    <Image
                      source={Images.passIcon}
                      style={passIconStyles}
                    />
                  </View>
                  <TextInput
                    autoCorrect={false}
                    onFocus={() => this.setKeyboardOffset(-INPUT_HEIGHT)}
                    onBlur={() => this.setKeyboardOffset(0)}
                    onChangeText={text => this.handlePassword(text)}
                    secureTextEntry
                    placeholder={'password'.toUpperCase()}
                    placeholderTextColor={Colors.ghost}
                    value={this.state.passwordValue}
                    underlineColorAndroid={Colors.transparent}
                    style={[styles.input, styles.passwordInput, passwordValue && {fontFamily: Fonts.type.bold}]}
                  />
                </View>
                <Options
                  checkboxText='Remember me'
                  checkboxHandle={this.handleRememberPress}
                  isChecked={this.state.isRemember}
                  buttonText='Forgotten Password'
                  buttonHandle={this.showForgotModal}
                />
                <View style={styles.submitContainer}>
                  <TouchableOpacity
                    onPress={this.handleLogin}
                    style={[styles.submitTouch, (!isCredentials || this.props.fetching) && {backgroundColor: Colors.bombay}]}
                  >
                    <Text style={styles.submitText}>
                      {'login'.toUpperCase()}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </KeyboardAvoidingView>
        </ScrollView>
        <Modal
          visible={this.state.isForgotModalVisible}
          transparent
          animationType='fade'
          onRequestClose={this.hideForgotModal}
        >
          <ForgotPasswordModal
            onSubmit={this.props.resetPassword}
            onClose={this.hideForgotModal}
          />
        </Modal>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    fetching: state.login.fetching,
    isRemember: state.login.isRemember,
    email: state.login.email,
    showTutorial: state.tutorial.isDisplay
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    attemptLogin: (email, password, isRemember, onFailure, nextPage) =>
      dispatch(LoginActions.loginRequest(email, password, isRemember, onFailure, nextPage)),
    resetPassword: (email) => dispatch(LoginActions.resetPassword(email)),
    getListItems: () => dispatch(ListItemsActions.listItemsRequest()),
    getDiseaseById: (id) => dispatch(DiseaseActions.diseaseRequest(id)),
    getFindings: () => dispatch(FindingsActions.findingsRequest()),
    getUnits: () => dispatch(UnitsActions.unitsRequest()),
    getFindingOutputs: () => dispatch(FindingOutputsActions.findingOutputsRequest())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
