// @flow

// Text paragraph in ChartViewDiagnose

import React from 'react'
import { View, Text, TouchableOpacity, Linking, Alert } from 'react-native'
import styles from './Styles/ChartViewParagraphsStyle'

export default class ChartViewParagraphs extends React.Component {
  getParagraphs = () => {
    const {titles, data} = this.props

    return Object.keys(titles).map((key, i) => {
      if (typeof titles[key] === 'string') {
        return (
          <View key={i}>
            <Text style={styles.title}>
              {titles[key]}
            </Text>
            {
              (data[key])
              ? <Text style={styles.text}>
                {data[key]}
              </Text>
              : <View>
                {
                  data.length > 0 && data.map((link) => {
                    return (
                      <TouchableOpacity key={link.id} style={styles.linkContainer} onPress={() => this.openLink(link.url)}>
                        <View style={styles.dot} />
                        <Text style={styles.linkText}>{link.title}</Text>
                      </TouchableOpacity>
                    )
                  })
                }
              </View>

            }
          </View>
        )
      } else {
        return (
          <View key={i}>
            <Text style={styles.title}>
              {titles[key].title}
            </Text>
            {Object.keys(titles[key].subTitles).map((subKey, j) => {
              return (
                <View key={j}>
                  <Text style={styles.subTitle}>
                    {titles[key].subTitles[subKey]}
                  </Text>
                  <Text style={styles.text}>
                    {data[subKey]}
                  </Text>
                </View>
              )
            })}
          </View>
        )
      }
    })
  }

  openLink (url) {
    Linking.canOpenURL(url)
    .then(supported => {
      if (!supported) {
        Alert.alert(`Can't handle url: ${url}`)
      } else {
        Linking.openURL(url)
      }
    }).catch(err => Alert.alert('An unexpected error happened', err))
  }

  render () {
    return (
      <View style={styles.container}>
        {this.getParagraphs()}
      </View>
    )
  }
}
