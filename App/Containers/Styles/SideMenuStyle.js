// @flow

import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Metrics.doublePadding,
    backgroundColor: Colors.danube,
    alignItems: 'center'
  },
  itemContainer: {
    alignItems: 'center'
  },
  item: {
    color: Colors.white,
    fontFamily: Fonts.type.light,
    fontSize: Fonts.size.h5,
    paddingVertical: Metrics.midPadding
  },
  separator: {
    ...Metrics.size(1, 35),
    backgroundColor: Colors.spindle
  }
})
