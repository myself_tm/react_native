// @flow

import { StyleSheet } from 'react-native'
import { Colors, Fonts, Metrics } from '../../Themes'

export default StyleSheet.create({
  scrollContainer: {
    flex: 1,
    backgroundColor: Colors.white
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start'
  },
  logoContainer: {
    flex: 0.55,
    justifyContent: 'center',
    alignItems: 'center'
  },
  loginContainer: {
    flex: 0.45,
    justifyContent: 'flex-end'
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: Metrics.padding,
    paddingHorizontal: Metrics.midPadding
  },
  emailInputContainer: {
    borderTopWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.bombay
  },
  emailIcon: {
    ...Metrics.size(24)
  },
  passIconContainer: {
    paddingHorizontal: Metrics.tinyPadding
  },
  passIcon: {
    ...Metrics.size(18, 24)
  },
  input: {
    flex: 1,
    height: Metrics.inputHeight,
    marginLeft: Metrics.doubleMargin,
    fontFamily: Fonts.type.light,
    fontSize: Fonts.size.input,
    color: Colors.tuna
  },
  emailInput: {},
  passInputContainer: {
    borderTopWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.bombay
  },
  passwordInput: {},
  submitContainer: {
    height: Metrics.normalize(70)
  },
  submitTouch: {
    flex: 1,
    backgroundColor: Colors.downy,
    justifyContent: 'center',
    alignItems: 'center'
  },
  submitText: {
    fontFamily: Fonts.type.semibold,
    fontSize: Fonts.size.submit,
    color: 'white'
  }
})
