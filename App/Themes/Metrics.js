// @flow

import { Dimensions, Platform } from 'react-native'
import { normalize } from '../Transforms/NormalizeStyles'

const { width, height } = Dimensions.get('window')

const IS_IOS = Platform.OS === 'ios'

// Used via Metrics.baseMargin
const metrics = {
  normalize: value => normalize(value),
  isIos: IS_IOS,

  // Padding
  tinyPadding: normalize(3),
  smallPadding: normalize(5),
  padding: normalize(10),
  midPadding: normalize(15),
  doublePadding: normalize(20),

  // Size
  size: (width, height = width) => {
    return {
      width: normalize(width),
      height: normalize(height)
    }
  },
  inputHeight: IS_IOS ? normalize(30) : normalize(48),
  regularLine: normalize(24),
  border: normalize(1),
  thickBorder: normalize(3),

  // Margin
  tinyMargin: normalize(3),
  smallMargin: normalize(5),
  margin: normalize(10),
  midMargin: normalize(15),
  doubleMargin: normalize(20),

  screenWidth: width < height ? width : height,
  screenHeight: width < height ? height : width,
  navBarHeight: (Platform.OS === 'ios') ? 64 : 54,
  buttonRadius: 4
}

export default metrics
