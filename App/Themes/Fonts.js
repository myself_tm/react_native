// @flow

// import { Dimensions } from 'react-native'
import { normalize } from '../Transforms/NormalizeStyles'
import Metrics from './Metrics'

const type = {
  base: Metrics.isIos ? 'OpenSans' : 'OpenSans-Regular',
  semibold: 'OpenSans-Semibold',
  bold: 'OpenSans-Bold',
  italic: 'OpenSans-Italic',
  light: 'OpenSans-Light',
  sub: 'Raleway-Regular',
  subSemibold: 'Raleway-Semibold'
}

const size = {
  h1: normalize(38),
  h2: normalize(34),
  h3: normalize(30),
  h4: normalize(26),
  h5: normalize(20),
  h6: normalize(19),
  input: normalize(20),
  submit: normalize(22),
  regular: normalize(17),
  medium: normalize(14),
  small: normalize(12),
  tiny: normalize(8.5),
  stepsText: normalize(17)
}

const style = {
  h1: {
    fontFamily: type.base,
    fontSize: size.h1
  },
  h2: {
    fontWeight: 'bold',
    fontSize: size.h2
  },
  h3: {
    fontFamily: type.emphasis,
    fontSize: size.h3
  },
  h4: {
    fontFamily: type.base,
    fontSize: size.h4
  },
  h5: {
    fontFamily: type.base,
    fontSize: size.h5
  },
  h6: {
    fontFamily: type.emphasis,
    fontSize: size.h6
  },
  normal: {
    fontFamily: type.base,
    fontSize: size.regular
  },
  description: {
    fontFamily: type.base,
    fontSize: size.medium
  }
}

export default {
  type,
  size,
  style
}

