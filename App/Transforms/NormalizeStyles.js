// @flow

import { Dimensions } from 'react-native'

const WIDTH_POINT = 480
export const scale = Dimensions.get('window').width / WIDTH_POINT

export const normalize = (size: number): number => {
  if (scale > 1) {
    return Math.round(size * scale)
  } else {
    return size
  }
}
