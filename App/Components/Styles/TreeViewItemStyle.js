// @flow

import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.whisper
  },
  labelContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  labelSubContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  leftRightContainer: {
    // width: 40,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  leftRight: {
    paddingVertical: Metrics.padding,
    paddingRight: Metrics.midPadding,
    fontFamily: Fonts.type.light,
    fontSize: Fonts.size.medium,
    color: Colors.manatee
  },
  label: {
    // width: Metrics.screenWidth / 2,
    color: Colors.balticSea,
    fontFamily: Fonts.type.base,
    fontSize: Fonts.size.regular
  },
  childrenCount: {
    color: Colors.danube
  },
  selectIconTouch: {
    padding: Metrics.midPadding
  },
  selectIcon: {
    ...Metrics.size(16)
  },
  expandIconTouch: {
    padding: Metrics.midPadding,
    paddingRight: Metrics.doublePadding,
    flex: 1,
    alignItems: 'flex-end'
  },
  expandIcon: {
    ...Metrics.size(10, 6)
  },
  expandIconActive: {
    tintColor: Colors.danube,
    transform: [{rotate: '180deg'}]
  },
  icon: {
    ...Metrics.size(16),
    marginRight: Metrics.margin
  },
  iconTouch: {
    paddingVertical: Metrics.smallPadding,
    paddingHorizontal: Metrics.padding,
    flexDirection: 'row',
    alignItems: 'center'
  },
  userInputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    position: 'relative'
  },
  enterButton: {
    paddingVertical: Metrics.smallPadding,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.downy,
    position: 'absolute',
    top: Metrics.normalize(-45),
    left: 0,
    right: 0
  },
  enterButtonText: {
    fontFamily: Fonts.type.semibold,
    fontSize: Fonts.size.medium,
    color: Colors.white
  },
  userInput: {
    paddingRight: Metrics.midPadding,
    height: Metrics.inputHeight,
    width: Metrics.normalize(90),
    fontFamily: Fonts.type.light,
    color: Colors.manatee,
    fontSize: Fonts.size.regular,
    textAlign: 'right'
  },
  unitText: {
    width: Metrics.normalize(45),
    fontFamily: Fonts.type.light,
    fontSize: Fonts.size.medium,
    color: Colors.manatee
  }
})
