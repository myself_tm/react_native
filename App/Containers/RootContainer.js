// @flow

import React, { Component } from 'react'
import { View, StatusBar } from 'react-native'
import NavigationRouter from '../Navigation/NavigationRouter'
import { connect } from 'react-redux'
import StartupActions from '../Redux/StartupRedux'
import ReduxPersist from '../Config/ReduxPersist'

// Styles
import styles from './Styles/RootContainerStyle'

class RootContainer extends Component {
  constructor () {
    super()

    this.state = {
      initialScreen: null,
      isDisplay: true
    }
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.isRemember) {
      if (nextProps.isDisplay) {
        this.setState({
          initialScreen: 'tutorial',
          isDisplay: true
        })
      } else {
        this.setState({
          initialScreen: 'examination',
          isDisplay: false
        })
      }
    } else {
      this.setState({
        initialScreen: 'login',
        isDisplay: true
      })
    }
  }

  componentDidMount () {
    // if redux persist is not active fire startup action
    if (!ReduxPersist.active) {
      this.props.startup()
    }
  }

  render () {
    if (this.state.initialScreen === null) {
      return null
    }

    return (
      <View style={styles.applicationView}>
        <StatusBar barStyle='light-content' />
        <NavigationRouter
          initialScreen={this.state.initialScreen}
          isDisplay={this.state.isDisplay}
        />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    fetching: state.login.fetching,
    isRemember: state.login.isRemember,
    isDisplay: state.tutorial.isDisplay,
    state
  }
}

// wraps dispatch to create nicer functions to call within our component
const mapDispatchToProps = (dispatch) => ({
  startup: () => dispatch(StartupActions.startup())
})

export default connect(mapStateToProps, mapDispatchToProps)(RootContainer)
