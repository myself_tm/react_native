// @flow

// Item in TreeViewList

import React from 'react'
import {
  View,
  Text,
  Image,
  Modal,
  Keyboard,
  TextInput,
  StyleSheet,
  TouchableOpacity
} from 'react-native'
import styles from './Styles/TreeViewItemStyle'
import { Images, Metrics, Fonts, Colors } from '../Themes'
import { highlight } from '../Transforms/StringManipulation'
import FindingModal from './FindingModal'
import FindingLeftRight from './FindingLeftRight'

export default class TreeViewItem extends React.Component {
  constructor (props) {
    super(props)

    this.data = props.data
    this.isActive = this.checkIsActive(props)
    this.isUserInput = this.checkIsUserInput(props)

    this.state = {
      isFocused: false,
      // userInput: props.diagnoseFindings[props.data.id] && props.diagnoseFindings[props.data.id].userInput || null,
      isExpanded: false,
      isModalVisible: false,
      isLeftRightShown: false,
      isBigTitle: false
    }
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.expandedParentItemId !== this.props.data.id) {
      if (this.props.parent && this.state.isExpanded) {
        this.setState({isExpanded: false})
      }
    }

    const currentIsActive = this.checkIsActive(this.props)
    const nextIsActive = this.checkIsActive(nextProps)
    if (currentIsActive !== nextIsActive) {
      this.isActive = nextIsActive
    }

    const currentIsUserInput = this.checkIsUserInput(this.props)
    const nextIsUserInput = this.checkIsUserInput(nextProps)
    if (currentIsUserInput !== nextIsUserInput) {
      this.isUserInput = nextIsUserInput
    }
  }

  checkIsActive = (props) => {
    const {diagnoseFindings} = props
    return !!diagnoseFindings[this.data.id]
  }

  checkIsUserInput = (props) => {
    const {diagnoseFindings, data} = props
    return diagnoseFindings[data.id] &&
      diagnoseFindings[data.id].userInput || false
  }

  hideModal = () => {
    this.setState({isModalVisible: false})
  }

  handleExpand = () => {
    this.setState({isExpanded: !this.state.isExpanded}, () => {
      if (this.state.isExpanded && this.props.parent) {
        this.props.expandParentItem(this.props.data.id)
      } else if (!this.state.isExpanded && this.props.parent) {
        this.props.expandParentItem(null)
      }
    })
  }

  getExpandIcon = () => {
    const {isExpanded} = this.state
    const {leftRight} = this.props.data

    if (this.props.children) {
      return (
        <TouchableOpacity
          onPress={() => this.handleExpand()}
          style={[styles.expandIconTouch, leftRight && {flex: 0}]}
        >
          <Image
            source={Images.expand}
            style={[styles.expandIcon, isExpanded && styles.expandIconActive]}
          />
        </TouchableOpacity>
      )
    } else {
      return null
    }
  }

  getLabel = () => {
    if (!!this.props.search && this.props.title === 'parent') {
      const {start, middle, end} = highlight(this.props.search, this.props.data.label)
      return (
        <Text numberOfLines={1}>
          {start.trim()}
          <Text style={{fontFamily: Fonts.type.bold}}>
            {middle}
          </Text>
          {end}
        </Text>
      )
    } else {
      return this.props.data.label
    }
  }

  getSortedOptions = () => {
    if (this.props.options) {
      let {options} = this.props
      options.sort((a, b) => {
        if (a.order > b.order) return 1
        if (a.order < b.order) return -1
      })
      return options
    } else {
      return null
    }
  }

  getMarginLeft = () => {
    let marginLeft

    switch (this.props.title) {
      case 'parent':
        marginLeft = 0
        break
      case 'child':
        marginLeft = Metrics.margin
        break
      case 'grand':
        marginLeft = Metrics.midMargin
        break
      case 'great':
        marginLeft = Metrics.doubleMargin
        break
      case '2-great':
        marginLeft = Metrics.doubleMargin + Metrics.smallMargin
        break
      default:
        marginLeft = 0
    }

    return {marginLeft}
  }

  handleItemSelect = (onCheckbox, onTitle) => {
    const {progenitorsIds} = this.props
    const {parentId} = this.data
    const options = this.getSortedOptions()

    if (this.data.userInput) {
      this.handleItemWithUserInput()
      return
    }

    if (!this.isActive) {
      if (options && onCheckbox) {
        this.props.selectFinding({
          [this.data.id]: {
            options: [options[0].id],
            progenitorsIds,
            parentId
          }
        })
      } else if (options) {
        this.setState({isModalVisible: true})
      } else {
        this.props.selectFinding({
          [this.data.id]: {
            progenitorsIds,
            parentId
          }
        })
      }
    } else if (this.isActive) {
      if (options && onTitle) {
        this.setState({isModalVisible: true})
      } else {
        this.props.deselectFinding(this.data.id)
      }
    }

    Keyboard.dismiss()
  }

  getLeftRightView = () => {
    return this.props.data.leftRight && (
      <FindingLeftRight
        findingData={this.props.data}
        diagnoseFindings={this.props.diagnoseFindings}
        selectFinding={this.props.selectFinding}
        deselectFinding={this.props.deselectFinding}
        options={this.getSortedOptions()}
        isFindingActive={this.isActive}
        progenitorsIds={this.props.progenitorsIds}
      />
    )
  }

  getLeftRightTextButton = () => {
    return this.props.data.leftRight && (
      <TouchableOpacity style={styles.leftRightContainer} onPress={this.toggleLeftRight}>
        <Text style={styles.leftRight}>L/R</Text>
      </TouchableOpacity>
    )
  }

  toggleLeftRight = () => {
    this.setState({isLeftRightShown: !this.state.isLeftRightShown})
  }

  handleItemWithUserInput = () => {
    if (!this.isUserInput) {
      this.userInputRef.focus()
    } else if (this.isUserInput) {
      this.props.deselectFinding(this.data.id)
    }
  }

  handleUserInput = (text, unitName) => {
    const {progenitorsIds} = this.props

    if (text.length > 0) {
      this.props.selectFinding({
        [this.data.id]: {
          parentId: this.data.parentId,
          userInput: text.trim(),
          progenitorsIds,
          unitName
        }
      })
    } else {
      this.props.deselectFinding(this.data.id)
    }
  }

  getUserInput = () => {
    const {userInput} = this.state
    const unitName = this.props.units[this.props.data.unitId]

    return this.props.data.userInput && (
      <View style={styles.userInputContainer}>
        <TextInput
          ref={ref => (this.userInputRef = ref)}
          style={[styles.userInput, userInput && {fontFamily: Fonts.type.bold}]}
          underlineColorAndroid={Colors.transparent}
          keyboardType='numeric'
          placeholder='input'
          onFocus={() => {
            this.setState({isFocused: true})
            Metrics.isIos && this.props.scrollToInput(this.userInputRef)
          }}
          onBlur={() => {
            setTimeout(() => this.setState({ isFocused: false }), 200)
          }}
          onChangeText={(text) => this.handleUserInput(text, unitName)}
          value={this.state.userInput}
        />
        {this.state.isFocused && Metrics.isIos && (
          <TouchableOpacity
            onPress={Keyboard.dismiss}
            style={styles.enterButton}
          >
            <Text style={styles.enterButtonText}>
              {'enter'.toUpperCase()}
            </Text>
          </TouchableOpacity>
        )}
      </View>
    )
  }

  render () {
    const {isExpanded, isLeftRightShown} = this.state
    const {isOneOfChildSelected} = this.props
    // const padding = !this.props.children.length && {paddingVertical: Metrics.smallPadding}
    const borderBottom = {borderBottomWidth: StyleSheet.hairlineWidth, borderColor: Colors.bombay}
    const marginLeft = this.getMarginLeft()
    const selectIcon = this.isActive
      ? Images.checkBoxActive
      : Images.checkBox

    return (
      <View style={[styles.container, !isExpanded && borderBottom]}>
        <View style={[styles.labelContainer, isExpanded && borderBottom, marginLeft]}>
          <View style={styles.labelSubContainer}>
            <TouchableOpacity onPress={() => this.handleItemSelect(true, false)} style={styles.selectIconTouch}>
              <Image source={selectIcon} style={styles.selectIcon} />
            </TouchableOpacity>
            <Text
              style={[
                styles.label,
                (isExpanded || isOneOfChildSelected) && {fontFamily: Fonts.type.bold},
                this.state.isBigTitle && {width: Metrics.screenWidth / 2}
              ]}
              numberOfLines={1}
              onPress={() => this.handleItemSelect(false, true)}
              onLayout={(evt) => {
                // evt.nativeEvent.layout.width > Metrics.screenWidth / 2 && this.setState({})
                if (evt.nativeEvent.layout.width > Metrics.screenWidth / 2) {
                  this.setState({isBigTitle: true})
                }
              }}
            >
              {this.getLabel()}
              {this.props.data.leftRight}
            </Text>
            {this.getUserInput()}
          </View>
          {this.getLeftRightTextButton()}
          {this.getExpandIcon()}
        </View>
        {isLeftRightShown && this.getLeftRightView()}
        {isExpanded && this.props.children}

        <Modal
          visible={this.state.isModalVisible}
          transparent
          animationType='fade'
          onRequestClose={this.hideModal}
        >
          <FindingModal
            title={this.props.data.label}
            onSubmit={() => {}}
            onClose={this.hideModal}
            findingData={this.data}
            options={this.getSortedOptions()}
            selectFinding={this.props.selectFinding}
            deselectFinding={this.props.deselectFinding}
            diagnoseFindings={this.props.diagnoseFindings}
            progenitorsIds={this.props.progenitorsIds}
          />
        </Modal>
      </View>
    )
  }
}
