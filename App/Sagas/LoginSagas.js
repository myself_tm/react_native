import { Alert } from 'react-native'
import { call, put } from 'redux-saga/effects'
import LoginActions from '../Redux/LoginRedux'
import { Actions as NavigationActions } from 'react-native-router-flux'

// attempts to login
export function * login (api, action) {
  const { email, password, isRemember, onFailure, nextPage } = action
  const response = yield call(api.login, email, password)

  if (response.ok) {
    yield put(LoginActions.loginSuccess(response.data.userId, isRemember, email))
    nextPage === 'examination'
      ? NavigationActions.examination()
      : NavigationActions.tutorial()
  } else {
    yield put(LoginActions.loginFailure(response.data.error.message))
    onFailure()
  }
}

export function * resetPassword (api, action) {
  const { email } = action
  const response = yield call(api.resetPassword, email.toLowerCase())

  if (response.ok) {
    Alert.alert(
      '',
      'Password reset successfully',
      [{text: 'OK', onPress: () => {}}]
    )
  } else {
    Alert.alert(
      '',
      response.data.error.message,
      [{text: 'OK', onPress: () => {}}]
    )
  }
}
