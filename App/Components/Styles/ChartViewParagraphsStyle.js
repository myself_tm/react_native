// @flow

import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  title: {
    marginBottom: Metrics.margin,
    color: Colors.danube,
    fontFamily: Fonts.type.semibold,
    fontSize: Fonts.size.medium
  },
  subTitle: {
    marginBottom: Metrics.margin,
    color: Colors.balticSea,
    fontFamily: Fonts.type.semibold,
    fontSize: Fonts.size.small
  },
  text: {
    marginBottom: Metrics.margin,
    color: Colors.manatee,
    fontFamily: Fonts.type.base,
    fontSize: Fonts.size.medium
  },
  dot: {
    ...Metrics.size(6),
    backgroundColor: Colors.danube,
    borderRadius: Metrics.buttonRadius
  },
  linkText: {
    color: Colors.danube,
    fontFamily: Fonts.type.base,
    fontSize: Fonts.size.small,
    textDecorationLine: 'underline',
    marginLeft: Metrics.margin
  },
  linkContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: Metrics.margin
  }
})
