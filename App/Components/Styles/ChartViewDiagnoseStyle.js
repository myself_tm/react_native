// @flow

import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  tabsContainer: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  tabContainer: {
    flexGrow: 1,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.ghost,
    borderTopWidth: 0,
    borderLeftWidth: 0
  },
  tabTouch: {
    alignItems: 'center',
    padding: Metrics.padding
  },
  tabTouchActive: {
    backgroundColor: Colors.whisper
  },
  tab: {
    color: Colors.manatee,
    fontFamily: Fonts.type.base,
    fontSize: Fonts.size.small
  },
  tabActive: {
    color: Colors.danube
  },
  contentContainer: {
    flex: 1,
    padding: Metrics.midPadding
  }
})
