// @flow

import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  sectionTextContainer: {
    padding: Metrics.midPadding
  },
  title: {
    marginBottom: Metrics.margin,
    color: Colors.balticSea,
    fontFamily: Fonts.type.base,
    fontSize: Fonts.size.regular
  },
  name: {
    color: Colors.balticSea,
    fontFamily: Fonts.type.base,
    fontSize: Fonts.size.medium
  },
  text: {
    color: Colors.manatee,
    fontFamily: Fonts.type.base,
    fontSize: Fonts.size.medium
  }
})
