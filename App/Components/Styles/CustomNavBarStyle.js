import { Colors, Fonts, Metrics } from '../../Themes/'

export default {
  navBar: {
    backgroundColor: Colors.balticSea,
    height: Metrics.normalize(50)
  },
  title: {
    color: Colors.white,
    fontFamily: Fonts.type.sub,
    fontSize: Fonts.size.h5
  }
}
