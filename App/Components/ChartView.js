// @flow

// Chart view in ExaminationScreen

import React from 'react'
import {
  View,
  Text,
  TouchableOpacity
} from 'react-native'
import styles from './Styles/ChartViewStyle'
import SectionHeader from './SectionHeader'
import ChartViewDiagnose from './ChartViewDiagnose'
import ChartViewFindings from './ChartViewFindings'

export default class ChartView extends React.Component {
  constructor () {
    super()

    this.state = {
      isDiagnoseCollapsed: true,
      isFindingsCollapsed: false,
      isPlanCollapsed: false,
      isDrugCollapsed: false
    }
  }

  getDiagnoseInfo = () => {
    if (!this.state.isDiagnoseCollapsed) {
      return (
        <ChartViewDiagnose
          data={this.props.disease}
          diseasesReference={this.props.diseasesReference}
          diseasesLinks={this.props.diseasesLinks}
        />
      )
    } else {
      return null
    }
  }

  getDiagnose = () => {
    return (
      <View>
        <TouchableOpacity onPress={() => this.setState({isDiagnoseCollapsed: !this.state.isDiagnoseCollapsed})}>
          <SectionHeader
            title='diagnose'
            label={this.props.title.toUpperCase()}
            selection={{}}
            removeSelection={() => {}}
            noAddButton
          />
        </TouchableOpacity>
        {this.getDiagnoseInfo()}
      </View>
    )
  }

  getFindingsInfo = () => {
    if (!this.state.isFindingsCollapsed) {
      return (
        <ChartViewFindings
          data={this.props.diagnoseFindings}
          findings={this.props.findings}
          listItems={this.props.listItems}
          findingOutputs={this.props.findingOutputs}
          disease={this.props.disease}
        />
      )
    } else {
      return null
    }
  }

  getFindings = () => {
    return (
      <View>
        <TouchableOpacity onPress={() => this.setState({isFindingsCollapsed: !this.state.isFindingsCollapsed})}>
          <SectionHeader
            title='findings'
            selection={{}}
            removeSelection={() => {}}
            noAddButton
          />
        </TouchableOpacity>
        {this.getFindingsInfo()}
      </View>
    )
  }

  getPlanningDescription = () => {
    if (!this.state.isPlanCollapsed) {
      const iDs = Object.keys(this.props.drugData)
      const toRender = iDs.map(id => {
        let items = []
        this.props.drugData[id].forEach(item => {
          if (item.treatmentPlanVariationTranslations) {
            item.treatmentPlanVariationTranslations.forEach(translate => {
              if (translate.locale === 'en') {
                items.push(translate.planText)
              }
            })
          } else {
            items.push(item.name)
          }
        })

        return items.join(' ')
      })

      return (
        <View style={styles.sectionTextContainer}>
          <Text style={styles.text}>
            {toRender.join(' ')}
          </Text>
        </View>
      )
    } else {
      return null
    }
  }

  getPlanning = () => {
    return (
      <View>
        <TouchableOpacity onPress={() => this.setState({isPlanCollapsed: !this.state.isPlanCollapsed})}>
          <SectionHeader
            title='plans'
            selection={{}}
            removeSelection={() => {}}
            noAddButton
          />
        </TouchableOpacity>
        {this.getPlanningDescription()}
      </View>
    )
  }

  getDrugDescription = () => {
    if (!this.state.isDrugCollapsed && Object.keys(this.props.diagnoseDrugs).length > 0) {
      const {diagnoseDrugs, drugData} = this.props
      const _drugData = {} // normalized drugData by id
      Object.keys(drugData).forEach((topId) => {
        drugData[topId].forEach((item) => {
          const itemId = item.treatmentPlanVariations
            ? item.treatmentPlanVariations[0].id
            : item.id
          _drugData[topId] = {
            ..._drugData[topId],
            [itemId]: item
          }
        })
      })

      const toRender = Object.keys(diagnoseDrugs).map((id, i) => {
        if (_drugData[id] && _drugData[id][diagnoseDrugs[id].id] && _drugData[id][diagnoseDrugs[id].id].treatmentPlanDrugVariation) {
          return (
            <View key={i}>
              <Text>
                <Text style={[styles.name, {fontWeight: 'bold'}]}>
                  {_drugData[id][diagnoseDrugs[id].id].treatmentPlanDrugVariation.drugVariation.drug.name}{' '}
                  ({_drugData[id][diagnoseDrugs[id].id].treatmentPlanDrugVariation.drugVariation.drug.activeSubstance})
                </Text>
                <Text style={styles.name}>
                  {' '}{_drugData[id][diagnoseDrugs[id].id].treatmentPlanDrugVariation.drugVariation.strength}
                  {_drugData[id][diagnoseDrugs[id].id].treatmentPlanDrugVariation.drugVariation.unit.name}{', '}
                  {_drugData[id][diagnoseDrugs[id].id].treatmentPlanDrugVariation.drugVariation.packageSize}{' '}
                  {_drugData[id][diagnoseDrugs[id].id].treatmentPlanDrugVariation.drugVariation.drugForm.name}
                </Text>
              </Text>
              {diagnoseDrugs[id].values && diagnoseDrugs[id].values.length &&
                <Text style={styles.text}>
                  Dosage:{diagnoseDrugs[id].values.map((value, i) => {
                    if (value) {
                      return <Text key={i}>{` ${value}`}</Text>
                    } else {
                      return <Text key={i}>{` 0`}</Text>
                    }
                  })}
                  {diagnoseDrugs[id].dose
                  ? <Text style={styles.text}>
                    {` / ${diagnoseDrugs[id].dose}`}
                  </Text>
                  : null
                  }
                </Text>
              }
            </View>
          )
        }
      })

      return (
        <View style={styles.sectionTextContainer}>
          {toRender}
        </View>
      )
    } else {
      return null
    }
  }

  getDrug = () => {
    return (
      <View>
        <TouchableOpacity onPress={() => this.setState({isDrugCollapsed: !this.state.isDrugCollapsed})}>
          <SectionHeader
            title='drugs'
            selection={{}}
            removeSelection={() => {}}
            noAddButton
          />
        </TouchableOpacity>
        {this.getDrugDescription()}
      </View>
    )
  }

  render () {
    return (
      <View style={styles.container}>
        {this.getDiagnose()}
        {this.getFindings()}
        {this.getPlanning()}
        {this.getDrug()}
      </View>
    )
  }
}
