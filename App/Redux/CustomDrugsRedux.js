// @flow

// Fetches and stores drugs data when user searches custom drug

import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  customDrugsRequest: ['search'],
  customDrugsSuccess: ['data'],
  customDrugsFailure: ['error'],
  customDrugsInitial: null
})

export const CustomDrugsTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: [],
  error: null,
  fetching: false
})

/* ------------- Reducers ------------- */

export const request = (state: Object) => state.merge({ fetching: true })

export const success = (state: Object, { data }: Object) =>
  state.merge({ fetching: false, error: null, data })

export const failure = (state: Object, { error }: Object) =>
  state.merge({ fetching: false, error })

export const initial = (state: Object) => INITIAL_STATE

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.CUSTOM_DRUGS_REQUEST]: request,
  [Types.CUSTOM_DRUGS_SUCCESS]: success,
  [Types.CUSTOM_DRUGS_FAILURE]: failure,
  [Types.CUSTOM_DRUGS_INITIAL]: initial
})

/* ------------- Selectors ------------- */
