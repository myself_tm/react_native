// @flow

export const capitalize = (str: string): string => {
  const _str = str.toLowerCase()
  const firstLetter = _str.slice(0, 1).toUpperCase()
  const leftOver = _str.slice(1)
  return firstLetter + leftOver
}

export const highlight = (search = '', string = ''): Object => {
  const _search = search.toLowerCase().trim()
  const _string = string.toLowerCase().trim()
  const pos = _string.indexOf(_search)
  let start = ''
  let middle = ''
  let end = ''

  if (pos === 0) {
    middle = capitalize(_string.slice(pos, pos + _search.length))
    end = _string.slice(pos + _search.length)
  } else if (pos === -1) {
    start = capitalize(_string)
  } else {
    start = capitalize(_string.slice(0, pos))
    middle = _string.slice(pos, pos + _search.length)
    end = _string.slice(pos + _search.length)
  }

  return {start, middle, end}
}
