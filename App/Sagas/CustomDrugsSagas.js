import { call, put } from 'redux-saga/effects'
import CustomDrugsActions from '../Redux/CustomDrugsRedux'

export function * getCustomDrugs (api, action) {
  const response = yield call(api.getCustomDrugs, action.search)

  if (response.ok) {
    yield put(CustomDrugsActions.customDrugsSuccess(response.data))
  } else {
    yield put(CustomDrugsActions.customDrugsFailure(response.data.error.message))
  }
}
