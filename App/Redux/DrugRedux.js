// @flow

// Stores drugs data by user selection
// Drugs displayed in the Examination screen renders based on this data
// Probably redundant

import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  drugAdd: ['data'],
  drugRemove: ['planId', 'id'],
  drugAddCustom: ['data'],
  drugRemoveCustom: ['id'],
  drugDeselect: null,
  drugDeselectAll: null
})

export const DrugTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: {},
  dataCustom: {}
})

/* ------------- Reducers ------------- */

export const add = (state: Object, { data }: Object) => {
  const id = Object.keys(data)[0]
  return state.merge({ data: {...state.data, [id]: [data[id]]} })
}

export const remove = (state: Object, { planId, id }: Object) => {
  const _data = state.data[planId].filter(item => item.id !== id)
  if (!_data.length) {
    return state.merge({ data: Immutable.without(state.data, planId) })
  }
  return state.merge({ data: {...state.data, [planId]: _data} })
}

export const addCustom = (state: Object, { data }: Object) =>
  state.merge({ dataCustom: {...state.dataCustom, ...data} })

export const removeCustom = (state: Object, { id }: string) =>
  state.merge({ dataCustom: Immutable.without(state.dataCustom, id) })

export const deselect = (state: Object, { id }: string) =>
  state.merge({ data: {} })

export const deselectAll = (state: Object, { id }: string) => INITIAL_STATE

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.DRUG_ADD]: add,
  [Types.DRUG_REMOVE]: remove,
  [Types.DRUG_ADD_CUSTOM]: addCustom,
  [Types.DRUG_REMOVE_CUSTOM]: removeCustom,
  [Types.DRUG_DESELECT]: deselect,
  [Types.DRUG_DESELECT_ALL]: deselectAll
})

/* ------------- Selectors ------------- */

