// @flow

const colors = {
  transparent: 'rgba(0,0,0,0)',
  translucentWhite: 'rgba(255, 255, 255, 0.8)',
  white: '#ffffff',
  balticSea: '#1e1d22',
  whisper: '#f6f7fa',
  bombay: '#b0b3ba',
  tuna: '#3f3f4b',
  downy: '#59ceb9',
  ghost: '#c3c6ce',
  manatee: '#92919d',
  danube: '#5990ce',
  roman: '#db5050',
  bastille: '#2c2b33',
  buttercup: '#f5a623',
  purple: '#a86adf',
  nevada: '#6d7076',
  quartz: '#eaf1f9',
  spindle: '#acc8e6'
}

export default colors
