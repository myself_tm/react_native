import { call, put } from 'redux-saga/effects'
import FindingsActions from '../Redux/FindingsRedux'

export function * getDrug (api, action) {
  const response = yield call(api.getFindings)

  if (response.ok) {

    // yield put(FindingsActions.findingsSuccess(normalizedData))
  } else {
    yield put(FindingsActions.findingsFailure())
  }
}
