import { takeLatest } from 'redux-saga'
import API from '../Services/Api'
import FixtureAPI from '../Services/FixtureApi'
import DebugSettings from '../Config/DebugSettings'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
import { UnitsTypes } from '../Redux/UnitsRedux'
import { LoginTypes } from '../Redux/LoginRedux'
import { OpenScreenTypes } from '../Redux/OpenScreenRedux'
import { TutorialTypes } from '../Redux/TutorialRedux'
import { FindingsTypes } from '../Redux/FindingsRedux'
import { ListItemsTypes } from '../Redux/ListItemsRedux'
import { Icd10Types } from '../Redux/Icd10Redux'
import { DiseaseTypes } from '../Redux/DiseaseRedux'
import { PlanningTypes } from '../Redux/PlanningRedux'
import { CustomPlanningTypes } from '../Redux/CustomPlanningRedux'
import { CustomDrugsTypes } from '../Redux/CustomDrugsRedux'
import { FindingOutputsTypes } from '../Redux/FindingOutputsRedux'
import { FindingGroupsTypes } from '../Redux/FindingGroupsRedux'
import { DiseaseReferenceTypes } from '../Redux/DiseasesReferenceToDiseasesRedux'
import { DiseaseLinksTypes } from '../Redux/DiseasesLinksRedux'
import { UserStateTypes } from '../Redux/UserStateRedux'

/* ------------- Sagas ------------- */

import { startup } from './StartupSagas'
import { getUnits } from './UnitsSagas'
import { login, resetPassword } from './LoginSagas'
import { openScreen } from './OpenScreenSagas'
import { setDisplaying } from './TutorialSagas'
import { getFindings } from './FindingsSagas'
import { getListItems } from './ListItemsSagas'
import { getIcd10Items } from './Icd10Sagas'
import { getDiseaseById } from './DiseaseSagas'
import { getPlanning } from './PlanningSagas'
import { getCustomPlanning } from './CustomPlanningSagas'
import { getCustomDrugs } from './CustomDrugsSagas'
import { getFindingOutputs } from './FindingOutputsSagas'
import { getFindingGroups } from './FindingGroupsSagas'
import { getReferenceToDiseasesByDiseaseId } from './DiseasesReferenceToDiseasesSagas'
import { getLinksByDiseaseId } from './DiseasesLinksSagas'
import { sendUserState } from './UserStateSagas'

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = DebugSettings.useFixtures ? FixtureAPI : API.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield [
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),
    takeLatest(OpenScreenTypes.OPEN_SCREEN, openScreen),
    takeLatest(TutorialTypes.SET_DISPLAYING, setDisplaying),

    // some sagas receive extra parameters in addition to an action
    takeLatest(UnitsTypes.UNITS_REQUEST, getUnits, api),
    takeLatest(LoginTypes.LOGIN_REQUEST, login, api),
    takeLatest(LoginTypes.RESET_PASSWORD, resetPassword, api),
    takeLatest(FindingsTypes.FINDINGS_REQUEST, getFindings, api),
    takeLatest(ListItemsTypes.LIST_ITEMS_REQUEST, getListItems, api),
    takeLatest(Icd10Types.ICD10_REQUEST, getIcd10Items, api),
    takeLatest(DiseaseTypes.DISEASE_REQUEST, getDiseaseById, api),
    takeLatest(PlanningTypes.PLANNING_REQUEST, getPlanning, api),
    takeLatest(CustomPlanningTypes.CUSTOM_PLANNING_REQUEST, getCustomPlanning, api),
    takeLatest(CustomDrugsTypes.CUSTOM_DRUGS_REQUEST, getCustomDrugs, api),
    takeLatest(FindingGroupsTypes.FINDING_GROUPS_REQUEST, getFindingGroups, api),
    takeLatest(DiseaseReferenceTypes.DISEASE_REFERENCE_REQUEST, getReferenceToDiseasesByDiseaseId, api),
    takeLatest(DiseaseLinksTypes.DISEASE_LINKS_REQUEST, getLinksByDiseaseId, api),
    takeLatest(FindingOutputsTypes.FINDING_OUTPUTS_REQUEST, getFindingOutputs, api),
    takeLatest(UserStateTypes.USER_STATE_REQUEST, sendUserState, api)
  ]
}
