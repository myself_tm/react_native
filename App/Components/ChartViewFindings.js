// @flow

// Findings item in ChartView

import React from 'react'
import { View, Text } from 'react-native'
import styles from './Styles/ChartViewFindingsStyle'
import Immutable from 'seamless-immutable'

const EXTRA_SYMBOLS_G = /_\w@\d*:(.*?):\w_/g
const EXTRA_SYMBOLS = /_\w@\d*:(.*?):\w_/
const EXTRA_DOTS = /\.\./g

export default class ChartViewFindings extends React.Component {
  getOutput = (id, findingWithoutOptions, output, allOptions, label, userInput, unit) => {
    const {isLeft, isRight} = this.props.data[id]
    const {options, leftOptions, rightOptions} = allOptions
    const allComparedOptions = this.getComparedOptions(options, leftOptions, rightOptions)
    const {
      comparedOptions,
      comparedLeftOptions,
      comparedRightOptions,
      comparedBilaterally
    } = allComparedOptions
    const isOptions = !!comparedOptions.length
    const isLeftOptions = !!comparedLeftOptions.length
    const isRightOptions = !!comparedRightOptions.length
    const isBiOptions = !!comparedBilaterally.length
    const optionsToRender = this.getOptionsToRender(comparedOptions)
    const leftOptionsToRender = this.getOptionsToRender(comparedLeftOptions)
    const rightOptionsToRender = this.getOptionsToRender(comparedRightOptions)
    const bilaterallyToRender = this.getOptionsToRender(comparedBilaterally)
    const onLeft = 'on the left side'
    const onRight = 'on the right side'
    const bi = 'bilaterally'

    if (!userInput && findingWithoutOptions) {
      if (isLeft && isRight) {
        return `${output} bilaterally.`
      } else if (isLeft) {
        return `${output} ${onLeft}.`
      } else if (isRight) {
        return `${output} ${onRight}.`
      } else {
        return `${output}.`
      }
    } else if (!userInput && !findingWithoutOptions) {
      if (isOptions && isLeftOptions && isRightOptions && isBiOptions) {
        return `${output} ${optionsToRender}, ${leftOptionsToRender} ${onLeft}, ${rightOptionsToRender} ${onRight}, ${bilaterallyToRender} ${bi}`
      } else if (!isOptions && isLeftOptions && isRightOptions && isBiOptions) {
        return `${output} ${leftOptionsToRender} ${onLeft}, ${rightOptionsToRender} ${onRight} and ${bilaterallyToRender} ${bi}`
      } else if (isOptions && !isLeftOptions && isRightOptions && isBiOptions) {
        return `${output} ${optionsToRender}, ${rightOptionsToRender} ${onRight} and ${bilaterallyToRender} ${bi}`
      } else if (isOptions && isLeftOptions && !isRightOptions && isBiOptions) {
        return `${output} ${optionsToRender}, ${leftOptionsToRender} ${onLeft} and ${bilaterallyToRender} ${bi}`
      } else if (isOptions && isLeftOptions && isRightOptions && !isBiOptions) {
        return `${output} ${optionsToRender}, ${leftOptionsToRender} ${onLeft}, ${rightOptionsToRender} ${onRight}`
      } else if (!isOptions && !isLeftOptions && isRightOptions && isBiOptions) {
        return `${output} ${rightOptionsToRender} ${onRight}, ${bilaterallyToRender} ${bi}`
      } else if (!isOptions && isLeftOptions && !isRightOptions && isBiOptions) {
        return `${output} ${leftOptionsToRender} ${onLeft}, ${bilaterallyToRender} ${bi}`
      } else if (!isOptions && isLeftOptions && isRightOptions && !isBiOptions) {
        return `${output} ${leftOptionsToRender} ${onLeft}, ${rightOptionsToRender} ${onRight}`
      } else if (isOptions && !isLeftOptions && !isRightOptions && isBiOptions) {
        return `${output} ${optionsToRender}, ${bilaterallyToRender} ${bi}`
      } else if (isOptions && !isLeftOptions && isRightOptions && !isBiOptions) {
        return `${output} ${optionsToRender}, ${rightOptionsToRender} ${onRight}`
      } else if (isOptions && isLeftOptions && !isRightOptions && !isBiOptions) {
        return `${output} ${optionsToRender}, ${leftOptionsToRender} ${onLeft}`
      } else if (isOptions && !isLeftOptions && !isRightOptions && !isBiOptions) {
        return `${output} ${optionsToRender}`
      } else if (!isOptions && isLeftOptions && !isRightOptions && !isBiOptions) {
        return `${output} ${leftOptionsToRender} ${onLeft}`
      } else if (!isOptions && !isLeftOptions && isRightOptions && !isBiOptions) {
        return `${output} ${rightOptionsToRender} ${onRight}`
      } else if (!isOptions && !isLeftOptions && !isRightOptions && isBiOptions) {
        return `${output} ${bilaterallyToRender} ${bi}`
      }
    } else {
      return `${label}: ${userInput}${unit}.`
    }
  }

  getComparedOptions = (options, leftOptions, rightOptions) => {
    let _options = options
    let _leftOptions = leftOptions
    let _rightOptions = rightOptions
    let _bilaterally = []

    if (_options.length) {
      if (_leftOptions.length) {
        _options = _options.filter(a => !(_leftOptions.indexOf(a) + 1))
      }
      if (_rightOptions.length) {
        _options = _options.filter(a => !(_rightOptions.indexOf(a) + 1))
      }
    }

    if (_leftOptions.length && _rightOptions.length) {
      _leftOptions = _leftOptions.filter(a => {
        const pos = _rightOptions.indexOf(a)
        if (pos + 1) {
          _rightOptions.splice(pos, 1)
          _bilaterally = _bilaterally.concat(a)
          return false
        } else {
          return true
        }
      })
    }

    return {
      comparedOptions: _options,
      comparedLeftOptions: _leftOptions,
      comparedRightOptions: _rightOptions,
      comparedBilaterally: _bilaterally
    }
  }

  compareNums = (a, b) => {
    if (a > b) return 1
    if (a < b) return -1
  }

  getOptionsToRender = (options) => {
    return options.length > 1 ? options.join(', ') : options[0]
  }

  getOptions = (id, options) => {
    const {data} = this.props
    const sortedOptions = Immutable.asMutable(data[id][options]).sort(this.compareNums)

    return sortedOptions.map(optionId => {
      let output = ''
      this.props.listItems[id].forEach(optionData => {
        if (optionData.id === optionId) {
          output = optionData.output
        }
      })
      return output
    })
  }

  getFindingsInfo = () => {
    const {data, disease} = this.props

    const findingsOutput = Object.keys(data).map(id => {
      let finding = {}
      const _parentId = data[id].parentId

      if (_parentId !== null) {
        this.props.findings[_parentId].forEach(item => {
          if (item.id === +id) {
            finding = item
          }
        })
      } else {
        disease.symptoms.forEach(item => {
          if (item.findingId === +id) {
            finding = item.findings
          }
        })
      }

      const selectedOptionsArr = data[id].options
        ? this.getOptions(id, 'options')
        : []
      const selectedOptionsLeftArr = data[id].isLeftOptions
        ? this.getOptions(id, 'isLeftOptions')
        : []
      const selectedOptionsRightArr = data[id].isRightOptions
        ? this.getOptions(id, 'isRightOptions')
        : []
      const findingWithoutOptions = !selectedOptionsArr.length &&
          !selectedOptionsLeftArr.length && !selectedOptionsRightArr.length

      const namePos = (finding.name.indexOf('/') + 1 || finding.name.indexOf('-') + 1 || finding.name.indexOf(' ') + 1) - 1
      const name = finding.name.slice(0, namePos).trim()
      const findingOutput = this.props.findingOutputs[id]
        ? this.props.findingOutputs[id].output
        : ''
      const allSelectedOptions = {
        options: selectedOptionsArr,
        leftOptions: selectedOptionsLeftArr,
        rightOptions: selectedOptionsRightArr
      }
      const label = finding.label
      const userInput = data[id].userInput || ''
      const unit = data[id].unitName || ''
      let output = this.getOutput(
        id,
        findingWithoutOptions,
        findingOutput,
        allSelectedOptions,
        label,
        userInput,
        unit
      )

      const allMatches = output.match(EXTRA_SYMBOLS_G)
      if (allMatches !== null) {
        allMatches.forEach(item => {
          const _match = item.match(EXTRA_SYMBOLS)
          output = output.replace(_match[0], _match[1])
        })
      }

      output = output.replace(EXTRA_DOTS, '.')

      return {
        name,
        output
      }
    })

    let _data = {}

    findingsOutput.forEach(item => {
      if (_data.hasOwnProperty([item.name])) {
        _data[item.name] = `${_data[item.name]} ${item.output}`
      } else {
        _data[item.name] = item.output
      }
    })

    return Object.keys(_data).map((name, i) => {
      return (
        <View key={i}>
          <Text style={styles.name}>
            {name}
            <Text style={styles.text}>
              {` - ${_data[name]}`}
            </Text>
          </Text>
        </View>
      )
    })
  }

  render () {
    return (
      <View style={styles.sectionTextContainer}>
        {this.getFindingsInfo()}
      </View>
    )
  }
}

