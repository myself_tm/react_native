// @flow

// Main menu of the app

import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import SideMenu from 'react-native-side-menu'
import styles from './Styles/SideMenuStyle'
import { Images, Metrics, ApplicationStyles } from '../Themes'
import { Actions as NavigationActions } from 'react-native-router-flux'
import LoginActions from '../Redux/LoginRedux'

const MENU_ITEMS = ['dashboard', 'our story', 'settings', 'about', 'logout']
const MENU_OFFSET = 3 / 4

class SideMenuContainer extends React.Component {
  getMenuItems = () => {
    return MENU_ITEMS.map((item, i) => {
      return (
        <View key={i} style={styles.itemContainer}>
          <TouchableOpacity onPress={this.getHandler(item)}>
            <Text style={styles.item}>
              {item.toUpperCase()}
            </Text>
          </TouchableOpacity>
          {i < (MENU_ITEMS.length - 1) && <View style={styles.separator} />}
        </View>

      )
    })
  }

  getHandler = (item) => {
    switch (item) {
      case 'dashboard':
        return this._dashBoard
      case 'our story':
        return this._ourStory
      case 'logout':
        return this._logout
      case 'about':
        return this._about
      case 'settings':
        return this._settings
      default:
        return () => {}
    }
  }

  _dashBoard = () => {
    NavigationActions.examination()
  }

  _ourStory = () => {
    NavigationActions.ourStory()
  }

  _settings = () => {
    NavigationActions.settings()
  }

  _logout = () => {
    this.props.logout()
    NavigationActions.login()
  }

  _about = () => {
    NavigationActions.tutorial()
  }

  render () {
    return (
      <SideMenu
        menu={
          <View style={styles.container}>
            <Image source={Images.menuLogo} {...ApplicationStyles.menuLogo} />
            {this.getMenuItems()}
          </View>
        }
        openMenuOffset={Metrics.screenWidth * MENU_OFFSET}
        isOpen={this.props.isOpen}
        onChange={this.props.onChange}
      >
        {this.props.children}
      </SideMenu>
    )
  }
}

const mapStateToProps = (state) => {
  return {}
}

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(LoginActions.logout())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SideMenuContainer)
