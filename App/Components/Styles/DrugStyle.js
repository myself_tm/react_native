// @flow

import { StyleSheet } from 'react-native'
import { Metrics, Colors, Fonts } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: Metrics.midPadding,
    // paddingTop: Metrics.smallPadding,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.bombay
  },
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  title: {
    flex: 1,
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.base,
    color: Colors.balticSea
  },
  subTitleContainer: {},
  subTitle: {
    fontSize: Fonts.size.medium,
    color: Colors.manatee,
    marginBottom: Metrics.margin
  },
  inputsContainer: {
    paddingBottom: Metrics.doublePadding,
    paddingRight: Metrics.midPadding
  },
  inputsLine: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: Metrics.margin
  },
  input: {
    height: Metrics.inputHeight,
    borderRadius: Metrics.normalize(3),
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.manatee,
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.base,
    color: Colors.balticSea
  },
  inputValues: {
    width: Metrics.normalize(40),
    textAlign: 'center'
  },
  inputDose: {
    flex: 1,
    paddingHorizontal: Metrics.padding
  },
  expandIconTouch: {
    padding: Metrics.midPadding,
    paddingRight: Metrics.doublePadding
  },
  expandIcon: {
    ...Metrics.size(10, 6)
  },
  expandIconActive: {
    tintColor: Colors.danube,
    transform: [{rotate: '180deg'}]
  }
})
