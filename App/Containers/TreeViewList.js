// @flow

// List with Grouped findings fetches from api

import React from 'react'
import { View, ScrollView, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'
import styles from './Styles/TreeViewListStyle'
import FindingGroupsActions from '../Redux/FindingGroupsRedux'
import TreeViewItem from '../Components/TreeViewItem'
import { Metrics } from '../Themes'
import DiagnoseActions from '../Redux/DiagnoseRedux'

class TreeViewList extends React.Component {
  constructor () {
    super()

    this.state = {
      dataReceived: false,
      expandedParentItemId: null
    }
  }

  componentDidMount () {
    if (!this.props.dataReceived) {
      this.props.getFindingGroups(() => this.setState({dataReceived: true}))
    }
  }

  expandParentItem = (id) => {
    this.setState({expandedParentItemId: id})
  }

  checkIsOneOfChildSelected = (itemId) => {
    const {diagnoseFindings} = this.props

    return (
      Object.keys(diagnoseFindings).some(id => {
        return (
          diagnoseFindings[id] &&
          diagnoseFindings[id].progenitorsIds &&
          (diagnoseFindings[id].progenitorsIds.some(progenitorId => progenitorId === itemId))
        )
      })
    )
  }

  renderList = (finding, title, progenitorsIds) => {
    let _title
    let _progenitorsIds = progenitorsIds.concat(finding.id)
    switch (title) {
      case 'parent':
        _title = 'child'
        break
      case 'child':
        _title = 'grand'
        break
      case 'grand':
        _title = 'great'
        break
      case 'great':
        _title = '2-great'
        break
      case '2-great':
        _title = '3-great'
        break
      default:
        _title = '-----'
    }

    if ((finding.label.indexOf(this.props.search) + 1 && title === 'parent') || title !== 'parent') {
      const options = finding.id
        ? this.props.listItems[finding.id]
        : {}
      return (
        <TreeViewItem
          data={finding}
          units={this.props.units}
          title={title}
          key={finding.id}
          options={options}
          search={this.props.search}
          selectFinding={this.props.selectFinding}
          deselectFinding={this.props.deselectFinding}
          diagnoseFindings={this.props.diagnoseFindings}
          searchItemSelect={this.props.searchItemSelect}
          scrollToInput={this.props.scrollToInput}
          parent={title === 'parent'}
          expandParentItem={this.expandParentItem}
          expandedParentItemId={this.state.expandedParentItemId}
          progenitorsIds={_progenitorsIds}
          isOneOfChildSelected={this.checkIsOneOfChildSelected(finding.id)}
        >
          {!!this.props.findings[finding.id] &&
            this.props.findings[finding.id].map(child => this.renderList(child, _title, _progenitorsIds))
          }
        </TreeViewItem>
      )
    } else {
      return null
    }
  }

  render () {
    if (this.props.fetching) {
      return <ActivityIndicator size='large' style={{height: Metrics.normalize(100)}} />
    }

    return (
      <View style={styles.container}>
        <ScrollView keyboardShouldPersistTaps='always'>
          {this.props.dataReceived &&
            this.props.findingGroups.findings.map(finding => this.renderList(finding, 'parent', []))
          }
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    units: state.units.data,
    findingGroups: state.findingGroups.data,
    fetching: state.findingGroups.fetching,
    dataReceived: state.findingGroups.dataReceived,
    diagnoseFindings: state.diagnose.findings
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    selectFinding: (data) => dispatch(DiagnoseActions.diagnoseSelectFinding(data)),
    deselectFinding: (id) => dispatch(DiagnoseActions.diagnoseDeselectFinding(id)),
    getFindingGroups: (onSuccess) => dispatch(FindingGroupsActions.findingGroupsRequest(onSuccess))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TreeViewList)
