// @flow

// Provide user login
// Stores userId, user selection of remain logged in and user email
// Also stores in Async storage

import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  loginRequest: ['email', 'password', 'isRemember', 'onFailure', 'nextPage'],
  loginSuccess: ['userId', 'isRemember', 'email'],
  loginFailure: ['error'],
  logout: null,
  resetPassword: ['email']
})

export const LoginTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  userId: null,
  error: null,
  fetching: false,
  isRemember: false,
  email: null
})

/* ------------- Reducers ------------- */

// we're attempting to login
export const request = (state: Object) => state.merge({ fetching: true })

// we've successfully logged in
export const success = (state: Object, { userId, isRemember, email }: Object) =>
  state.merge({ fetching: false, error: null, userId, isRemember, email })

// we've had a problem logging in
export const failure = (state: Object, { error }: Object) =>
  state.merge({ fetching: false, error })

// we've logged out
export const logout = (state: Object) =>
  state.merge({ ...INITIAL_STATE, email: state.email })

export const resetPassword = (state: Object) => state

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.LOGIN_REQUEST]: request,
  [Types.LOGIN_SUCCESS]: success,
  [Types.LOGIN_FAILURE]: failure,
  [Types.LOGOUT]: logout,
  [Types.RESET_PASSWORD]: resetPassword
})

/* ------------- Selectors ------------- */

// Is the current user logged in?
export const isLoggedIn = (loginState: Object) => loginState.username !== null
